package example.GetLoginProofResult

claims := ocm.getLoginProofResult(input.requestId)

name = getName(claims)
given_name = getGivenName(claims)
family_name = getFamilyName(claims)
middle_name = getMiddleName(claims)
preferred_username = getPreferredUsername(claims)
gender = getGender(claims)
birthdate = getBirthdate(claims)
email = getEmail(claims)
sub = getSub(claims)
iss = getIss(claims)
roles = split(claims.Claims, "|") 
fedId = claims.FederationId
did = getIss(claims) 

email_verified {
	claims.email != ""
}

getName(c) = x {
	x = concat(" " , [c.prcFirstName, c.prcLastName])
}
getGivenName(c) = x {
	x = c.prcFirstName
}
getFamilyName(c) = x {
	x = c.prcLastName
}
getMiddleName(c) = x {
	x = c.prcMiddleName
}
getPreferredUsername(c) = x {
	x = c.prcPreferredUsername
}
getGender(c) = x {
	x = c.prcGender
}
getBirthdate(c) = x {
	x = c.prcBirthdate
}
getEmail(c) = x {
	x = c.email
}
getSub(c) = x {
	x = c.subjectDID
}
getIss(c) = x {
	x = c.issuerDID
}
